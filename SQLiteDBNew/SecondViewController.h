//
//  SecondViewController.h
//  SQLiteDBNew
//
//  Created by Sword Software on 23/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

NS_ASSUME_NONNULL_BEGIN

@interface SecondViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *myData;
    
}

@end

NS_ASSUME_NONNULL_END
