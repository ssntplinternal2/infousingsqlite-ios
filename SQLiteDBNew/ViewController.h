//
//  ViewController.h
//  SQLiteDBNew
//
//  Created by Sword Software on 22/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *user;

- (IBAction)addName:(id)sender;

@end

