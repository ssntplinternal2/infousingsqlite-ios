//
//  SecondViewController.m
//  SQLiteDBNew
//
//  Created by Sword Software on 23/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *imageNameArray;
    NSInteger arraylength;
}

@property (weak, nonatomic) IBOutlet UITableView *table;

@end

NSString *myDB2 = @"mydata2.db";

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arraySetUp];
    [self.table registerClass:[UITableViewCell self] forCellReuseIdentifier:@"Cell"];
    
    NSString *userN;
    NSString *paths= [self getWritableDBPath];
    
    const char *dbpath = [paths UTF8String];
    sqlite3_stmt *statement;
    static sqlite3 *database = nil;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT username FROM users",nil];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            NSMutableArray *names = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                const unsigned char *tmp1 = sqlite3_column_text(statement, 0);
                
                if (tmp1 == NULL)
                    userN = nil;
                else
                    userN = [[NSString alloc] initWithUTF8String:tmp1];
                
                if ([userN length] != 0)
                    [names addObject:userN];
            }
            imageNameArray = [names copy];
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
}

-(NSString *) getWritableDBPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:myDB2];
}

-(void)arraySetUp {
   
}

#pragma mark - UITableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [imageNameArray count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = imageNameArray[indexPath.row];
    return cell;
}

@end
