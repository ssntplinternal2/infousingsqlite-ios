//
//  ViewController.m
//  SQLiteDBNew
//
//  Created by Sword Software on 22/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

NSString *myDB = @"mydata2.db";

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


// To add new records
- (IBAction)addName:(id)sender {
    [self saveUserInDatabase];
}


-(void)saveUserInDatabase{
    [self createEditableCopyOfDatabaseIfNeeded];
    
    NSString *filePath = [self getWritableDBPath];
    sqlite3 *database;
    NSString *theU = self.user.text;
    
    if ([theU length] !=0) {
        if(sqlite3_open([filePath UTF8String], &database) == SQLITE_OK){
            const char *sqlStatement = "insert into users (username) VALUES (?)";
            sqlite3_stmt *compiledStatement;
            
            if (sqlite3_prepare_v2(database, sqlStatement,-1,&compiledStatement,NULL) == SQLITE_OK){
                sqlite3_bind_text(compiledStatement, 1, [theU UTF8String], -1, SQLITE_TRANSIENT);
            }
            
            if(sqlite3_step(compiledStatement)!= SQLITE_DONE){
                NSLog(@"Save Error: %s",sqlite3_errmsg(database));
            }
            sqlite3_finalize(compiledStatement);
        }
        sqlite3_close(database);
    }
    else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"message:@"Enter A Valid User Name"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}



-(void)createEditableCopyOfDatabaseIfNeeded{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:myDB];
    
    success = [fileManager fileExistsAtPath:writableDBPath];
    
    if(success)
        return;
    
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:myDB];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
}



-(NSString *) getWritableDBPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    return [documentsDir stringByAppendingPathComponent:myDB];
}

@end
